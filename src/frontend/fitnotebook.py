import tkinter as tk
import tkinter.ttk as ttk

from frontend.progresstab import ProgrossTab
from frontend.workouttab import WorkoutTab


class FitnessNotebook(tk.Notebook):

    def __init__(self, parent, **options):
        tk.Notebook.__init__(self, parent, **options)

        self.parent = parent
        self.previous_tab = 'None'
        self.current_tab = 'None'
        self._init_widgets()
        #self._bind_events()

    def _init_widgets(self):

        self.workout_tab = WorkoutTab(self)
        self.progress_tab = ProgrossTab(self)