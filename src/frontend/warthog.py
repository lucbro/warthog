import tkinter as tk
import tkinter as ttk

from frontend.fitnotebook import FitnessNotebook
from frontend.menubar import MenuBar


class Warthog(tk.Tk):

    def __init__(self, parent):
        tk.Tk.__init__(self, parent)

        self.title("Warthog v0.01")
        self._init_widgets()

    def _init_widgets(self):

        self.fit_notebook = FitnessNotebook(self)
        self.menu_bar = MenuBar(self)