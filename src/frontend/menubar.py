import tkinter as tk
import tkinter.ttk as ttk


class MenuBar(tk.Menu):

    def __init__(self, parent, fit_notebook):
        tk.Menu.__init__(self, parent)

        self.parent = parent
        self.fit_notebook = fit_notebook
        self._init_widgets()

    def _init_widgets(self):
  
        file_menu = tk.Menu(self, tearoff=0)
        file_menu.add_command(label='Open', accelerator='Ctrl+O', underline=0,
            command=self._open)
        file_menu.add_command(label='New', accelerator='Ctrl+N', underline=0,
            command=self._new)
        file_menu.add_command(label='Save', accelerator='Ctrl+S', underline=0,
            command=self._save)
        file_menu.add_command(label='Save As', acclerator='Ctrl+Shift+S',
            underline=5, command=self._save_sas)
        file_menu.add_command(label='Close', acclerator='Ctrl+L', underline=1, 
            command=self._close)
        file_menu.add_command(label='Quit', accelerator='Ctrl+Q', underline=0, 
            command=self._quit)
        self.add_cascade(lable='File', menu=file_menu)